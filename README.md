Ansible Role: Win-SED-ECP
=========

Эта роль установит ПО, необходимое для использования ЭЦП в СЭД УИС и настроит АРМ пользователя на ОС Windows x64. Версии ПО:
  - Mozilla Firefox ESR 60.3.0
  - Java 8 update 191
  - CryptoPro версии 4.0.9842
  - драйвер Rutoken версии 4.8.10.0
 
Иструкция для ручной настройки находится на портале ФКУ НИИИТ: 

http://kb.ipt.fsin.uis/projects/sed_users_docs/wiki/%D0%9F%D0%9E_%D0%BD%D0%B5%D0%BE%D0%B1%D1%85%D0%BE%D0%B4%D0%B8%D0%BC%D0%BE%D0%B5_%D0%B4%D0%BB%D1%8F_%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F_%D0%AD%D0%9F_(%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%8F_%D0%A1%D0%AD%D0%94_%D0%A3%D0%98%D0%A1_48)

Requirements
------------

None.

Role Variables
--------------

Доступные переменные перечислены вместе со значениями по умолчанию (см. `defaults/main.yml`).

В Mozilla Firefox создается профиль пользователя по умолчанию (```default.zip```), при желании его можно кастомизировать под свои задачи, подменить файл своим.
Дополнительную информацию можно посмотреть тут https://support.mozilla.org/ru/kb/profili-gde-firefox-hranit-vashi-zakladki-paroli-i

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - win_sed_ecp

License
-------

BSD

Author Information
------------------

Chubik Sergey, sergey.chubik@mail.ru.
Chubik Sergey, chubik@ekaterinburg.fsin.uis.
